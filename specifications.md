# Spécifications de l'application


## Application déployée :
- Le backend de l'application est géré par un serveur NodeJS.  
- Le frontend de l'application est écrit en HTML/CSS/JS.  
- L'application utilise socketIO côté client et serveur pour faciliter l'envoi d'informations en temps réel et la bi-directionnalité de ces informations.  
- Les images sont stockées localement sur le serveur.  
- Le but du jeu est de trouver l'artiste qui correspond à l'image. Une fois qu'un joueur a trouvé la bonne réponse, une alerte est envoyée à tous les joueurs et l'image change.  
