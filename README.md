Atelier DevOps-SysOps : Création d’applications dans une infrastructure Cloud

Groupe -> Loris, Jeremy, Juliette, Flavien & Brice

Contexte de l’atelier :

Dans le but de remporter un appel d’offres pour la réalisation d’un projet décrit ci-dessous, plusieurs équipes DevOps décident de réaliser un prototype répondant aux spécifications demandées. Ces projets nécessitant un déploiement dans une infrastructure de Cloud, plusieurs équipes SysOps proposent le déploiement et la gestion de cette infrastructure complexe.

[voir pdf pour l'ensemble du sujet]

Résultat attendu :

• Une description des spécifications de l’application

• Un court document décrivant l’ensemble des principes Devops/Sysops suivis dans le projet
et comment ceux-ci ont été implémentés

• Le ou les dépôts de code source utilisés, que ce soit pour la gestion du code source des applications métier, ou pour la gestion des configurations des infrastructures.
Si nécessaire, certains dépôts de code peuvent être partagés entre les différentes équipes.

## Création de l'image Docker

Construire l'image docker depuis la racine du projet :

`docker build --tag guesstify .`

## Utiliser l'image Docker

Récupérer l'image depuis DockerHub

`docker pull lorispercin/guesstify`

Lancer l'image. Guesstify utilise le port 5000, si ce port est déjà utilisé par une autre application, publier ce port vers un port libre de la machine hôte.

`docker run -d -p 5000:5000 --name guesstify lorispercin/guesstify`
